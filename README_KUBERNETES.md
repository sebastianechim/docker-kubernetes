# Kubernetes basic commands

## Install kubernetes
### Install Qemu-KVM
Qemu is a virtualization software.

### Install Minikube
Download, make it executable and move it to the bin folder.

### [Optional] Install Kubectl
Kubectl is the main command line client for k8s.
There are other alternatives.
We have a built-in version of kubectl inside Minikube


## Minikube commands

### Get status
minikube status

### Start/Stop Minikube
minikube start
minikube stop

### Idk
minikube kubectl

### Starts the UI
minikube dashboard


## Kubectl commands

### Get current nodes
kubectl get nodes
kubectl get pods [--all-namespaces]
kubectl get services
kubectl get configmap
kubectl get secret
kubectl get namespaces

### Create elements manually
kubectl create secret <secret_name>
kubectl create deployment <deployment_name> --image <image_name> --namespace <namespace_name>
kubectl create configmap< configmap_name>
kubectl create namespace <namespace_name>

### Create elements from configuration yml file
kubectl apply -f <file_name>.yml

### Delete elements 
kubectl delete secret <secret_name>
kubectl delete deployment <deployment_name>
kubectl delete configmap <configmap_name>
kubectl delete namespace <namespace_name>

### Scale the pod
kubectl scale -n <namespace_name> deployment <deployment_name> --replicas=<nr_of_replicas>

### Describe the pod
kubectl describe pod -n <namespace_name> <pod_name>

### Create an internal service (cluster ip)
kubectl create service -n <namespace_name> clusterip <cluster_name> --port <port_number>
kubectl create service -n <namespace_name> clusterip <cluster_name> --tcp=<port_number1>:<port_number2>

### Expose the deployment as a public service (node-service)
kubectl expose deployment <deployment_name> -n <namespace_name> --type=<port_type> --name=<service_name> --port <port_number> 
kubectl expose deployment my-deployment -n my-space --type=NodePort --name=node-service --port 80 

