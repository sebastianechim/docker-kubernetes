# Docker basic commands
## List images
docker images

## List container
docker ps -aq

## Pull an image
docker pull image
docker pull <image_name>:<tag>

## Web server commonly used with docker
docker pull nginx 

## Remove docker image
docker rmi <image_name>

## Start/Stop container
docker stop <container_name_or_id>
docker start <container_name_or_id>

## Remove container
docker rm <container_name_or_id>

## Run image / name the container / detach from terminal
docker run <image_name>
docker run --name <container_name> <image_name>
docker run --name <container_name> -d <image_name> 

## Bulk start/stop/remove
docker stop $(docker ps -q)
docker start $(docker ps -aq)
docker rm $(docker ps -aq)

docker exec 708526b8b120 curl "http://127.0.0.1:80"

## Create a container and map internal port 8080 of the machine to the port 80 inside the container
docker run --name seb_con -p 8080:80 -v /home/nobleprog/Desktop/docker-k8s/web:/usr/share/nginx/html -d nginx
docker run --name seb-web -p 8080:80 -v /home/nobleprog/Desktop/docker-k8s/web:/root -d nginx

## Commit a container
docker commit <container_name> <image_name>:<tag>

## Change tag
docker tag <existing_image_name>:<existing_tag> <new_image_name>:<new_tag>

## Push to image repository
docker push <full_repo_name>:<tag>

## Login to docker (With the http://hub.docker.com account)
docker login

## Build the image
docker build -t <image_name>:<tag> <path_to_dockerfile>
docker build -t sebastianechim/simple-html-page:build .

## Run database with the environment variables
docker run --name my-mongo -p 27017:27017 -v /home/nobleprog/Desktop/data:/data -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=root mongo

## Docker-compose
docker compose up